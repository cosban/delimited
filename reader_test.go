package delimited

import (
	"io"
	"strings"
	"testing"

	"github.com/cosban/assert"
)

func Test_readToDelimiter_no_delimiter(t *testing.T) {
	given_src := []byte("this is a bunch of text with no delimiter")
	given_delim := []byte(",")

	actual, _, total, found := readToDelimiter(given_src, nil, given_delim)

	var expected []byte = given_src
	assert.Equals(t, expected, actual)
	assert.Equals(t, len(given_src), total)
	assert.False(t, found)
}

func Test_readToDelimiter_end_delimiter(t *testing.T) {
	given_src := []byte("this is a bunch of text with no delimiter,")
	given_delim := []byte(",")

	actual, _, total, found := readToDelimiter(given_src, nil, given_delim)

	var expected []byte = given_src[:len(given_src)-1]
	assert.Equals(t, expected, actual)
	assert.Equals(t, len(given_src), total)
	assert.True(t, found)
}

func Test_readToDelimiter_begin_delimiter(t *testing.T) {
	given_src := []byte(",this is a bunch of text with no delimiter")
	given_delim := []byte(",")

	actual, _, total, found := readToDelimiter(given_src, nil, given_delim)

	var expected []byte
	assert.Equals(t, expected, actual)
	assert.Equals(t, 1, total)
	assert.True(t, found)
}

func Test_readToDelimiter_inner_delimiter(t *testing.T) {
	given_src := []byte("this, is a bunch of text with no delimiter")
	given_delim := []byte(",")

	actual, _, total, found := readToDelimiter(given_src, nil, given_delim)

	var expected []byte = []byte("this")
	assert.Equals(t, expected, actual)
	assert.Equals(t, len(expected)+1, total)
	assert.True(t, found)
}

func Test_readToDelimiter_no_defined_src(t *testing.T) {
	given_src := []byte("")
	given_delim := []byte(",")

	actual, _, total, found := readToDelimiter(given_src, nil, given_delim)

	var expected []byte
	assert.Equals(t, expected, actual)
	assert.Equals(t, len(expected), total)
	assert.False(t, found)
}

func Test_splitRow_no_delimiter(t *testing.T) {
	given_row := []byte("this is a bunch of text with no delimiter")
	given_delim := []byte(",")

	actual := splitRow(given_row, given_delim, nil)

	var expected []string = []string{string(given_row)}
	assert.Equals(t, expected, actual)
}

func Test_splitRow_delimiter_at_end(t *testing.T) {
	given_row := []byte("this is a bunch of text with no delimiter,")
	given_delim := []byte(",")

	actual := splitRow(given_row, given_delim, nil)

	var expected []string = []string{
		"this is a bunch of text with no delimiter",
		"",
	}
	assert.Equals(t, expected, actual)
}

func Test_splitRow_many_delimiters(t *testing.T) {
	given_row := []byte("this is a bunch of text with no delimiter")
	given_delim := []byte(" ")

	actual := splitRow(given_row, given_delim, nil)

	var expected []string = []string{
		"this", "is", "a", "bunch", "of",
		"text", "with", "no", "delimiter",
	}
	assert.Equals(t, expected, actual)
}

func Test_ReadRow(t *testing.T) {
	given := strings.NewReader("A'~'B'~'C'~'D'~'E#@#@#Z'~'Y'~'X'~'W'~'V#@#@#~~1'~'~~2'~'~~3'~'#@#@#")
	reader := NewReader(given)
	reader.FieldDelimiter = "'~'"
	reader.RowDelimiter = "#@#@#"

	actual, err := reader.ReadRow()

	expected := []string{"A", "B", "C", "D", "E"}
	assert.Nil(t, err)
	assert.Equals(t, expected, actual)
}

func Test_ReadRow_maintains_buffer(t *testing.T) {
	given := strings.NewReader("A'~'B'~'C'~'D'~'E#@#@#Z'~'Y'~'X'~'W'~'V#@#@#~~1'~'~~2'~'~~3'~'#@#@#")
	reader := NewReader(given)
	reader.FieldDelimiter = "'~'"
	reader.RowDelimiter = "#@#@#"

	reader.ReadRow()
	actual, err := reader.ReadRow()

	expected := []string{"Z", "Y", "X", "W", "V"}
	assert.Nil(t, err)
	assert.Equals(t, expected, actual)
}

func Test_ReadRows(t *testing.T) {
	given := strings.NewReader("A'~'B'~'C'~'D'~'E#@#@#Z'~'Y'~'X'~'W'~'V#@#@#~~1'~'~~2'~'~~3'~'#@#@#")
	reader := NewReader(given)
	reader.FieldDelimiter = "'~'"
	reader.RowDelimiter = "#@#@#"

	actual, err := reader.ReadRows(2)

	expected := [][]string{
		[]string{"A", "B", "C", "D", "E"},
		[]string{"Z", "Y", "X", "W", "V"},
	}
	assert.Nil(t, err)
	assert.Equals(t, expected, actual)
}

func Test_ReadAll(t *testing.T) {
	given := strings.NewReader("A'~'B'~'C'~'D'~'E#@#@#Z'~'Y'~'X'~'W'~'V#@#@#~~1'~'~~2'~'~~3'~'")
	reader := NewReader(given)
	reader.FieldDelimiter = "'~'"
	reader.RowDelimiter = "#@#@#"

	actual, err := reader.ReadAll()

	expected := [][]string{
		[]string{"A", "B", "C", "D", "E"},
		[]string{"Z", "Y", "X", "W", "V"},
		[]string{"~~1", "~~2", "~~3", ""},
	}
	assert.Nil(t, err)
	assert.Equals(t, expected, actual)
}

func Test_ReadAll_no_end_delimiter(t *testing.T) {
	given := strings.NewReader("A'~'B'~'C'~'D'~'E#@#@#Z'~'Y'~'X'~'W'~'V#@#@#~~1'~'~~2'~'~~3'~'")
	reader := NewReader(given)
	reader.FieldDelimiter = "'~'"
	reader.RowDelimiter = "#@#@#"

	actual, err := reader.ReadAll()

	expected := [][]string{
		[]string{"A", "B", "C", "D", "E"},
		[]string{"Z", "Y", "X", "W", "V"},
		[]string{"~~1", "~~2", "~~3", ""},
	}
	assert.Nil(t, err)
	assert.Equals(t, expected, actual)
}

func Test_ReadAll_with_small_buffersize(t *testing.T) {
	given := strings.NewReader("A'~'B'~'C'~'D'~'E#@#@#Z'~'Y'~'X'~'W'~'V#@#@#~~1'~'~~2'~'~~3'~~'~'")
	reader := NewReader(given)
	reader.FieldDelimiter = "'~'"
	reader.RowDelimiter = "#@#@#"
	reader.BufferSize = 5

	actual, err := reader.ReadAll()

	expected := [][]string{
		[]string{"A", "B", "C", "D", "E"},
		[]string{"Z", "Y", "X", "W", "V"},
		[]string{"~~1", "~~2", "~~3'~~", ""},
	}
	assert.Nil(t, err)
	assert.Equals(t, expected, actual)
}

func Test_NoLineDelimiter(t *testing.T) {
	given := strings.NewReader("A'~'B'~'C'~'D'~'E")
	reader := NewReader(given)
	reader.FieldDelimiter = "'~'"

	actual, err := reader.ReadRow()

	expected := []string{"A", "B", "C", "D", "E"}

	assert.Equals(t, io.EOF, err)
	assert.Equals(t, expected, actual)
}
