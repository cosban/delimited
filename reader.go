package delimited

import (
	"bytes"
	"errors"
	"io"
	"unicode"
)

const BUFFERLEN = 1024

type Reader struct {
	FieldDelimiter string
	RowDelimiter   string
	TrimFunc       func(rune) bool
	BufferSize     int

	buffer      []byte
	delimBuffer []byte
	r           io.Reader
}

func NewReader(r io.Reader) *Reader {
	return &Reader{
		FieldDelimiter: ",",
		RowDelimiter:   "\n",
		TrimFunc:       unicode.IsSpace,
		r:              r,
		BufferSize:     BUFFERLEN,
	}
}

func (r *Reader) ReadAll() ([][]string, error) {
	var records [][]string
	var readError error
	for readError == nil {
		var row []string
		row, readError = r.ReadRow()
		if len(row) > 0 {
			records = append(records, row)
		}
	}
	if readError != io.EOF {
		return nil, readError
	}
	if len(r.buffer) > 0 {
		row, _ := r.ReadRow()
		if len(row) > 0 {
			records = append(records, row)
		}
	}
	return records, nil
}

func (r *Reader) ReadRow() ([]string, error) {
	return r.readRowWithRemainder()
}

func (r *Reader) ReadRows(numRows int) ([][]string, error) {
	var rows [][]string
	for i := 0; i < numRows; i++ {
		row, err := r.ReadRow()
		if err != nil && err != io.EOF {
			return nil, err
		}
		rows = append(rows, row)
	}
	return rows, nil
}

func (r *Reader) readRowWithRemainder() ([]string, error) {
	if r.BufferSize < len(r.RowDelimiter) || r.BufferSize < len(r.FieldDelimiter) {
		return nil, errors.New("BufferSize must be larger than the delimiters")
	}
	var row []byte
	if len(r.buffer) > 0 {
		buffer, delimBuffer, processed, reached := readToDelimiter(r.buffer, r.delimBuffer, []byte(r.RowDelimiter))
		row = append(row, buffer...)
		r.delimBuffer = delimBuffer
		if reached {
			r.buffer = r.buffer[processed:]
			return splitRow(row, []byte(r.FieldDelimiter), r.TrimFunc), nil
		}
	}

	var bytesRead int
	var readErr error
	byteBuffer := make([]byte, r.BufferSize)
	for readErr == nil {
		bytesRead, readErr = r.r.Read(byteBuffer)
		buffer, delimBuffer, processed, reached := readToDelimiter(byteBuffer[:bytesRead], r.delimBuffer, []byte(r.RowDelimiter))
		if bytesRead > 0 {
			row = append(row, buffer...)
			r.delimBuffer = delimBuffer
		}
		if reached {
			r.buffer = byteBuffer[processed:bytesRead]
			return splitRow(row, []byte(r.FieldDelimiter), r.TrimFunc), nil
		}
	}
	bufferLen := len(r.buffer)
	r.buffer = nil
	r.delimBuffer = nil
	if bytesRead < r.BufferSize || (readErr == io.EOF && (bytesRead > 0 || bufferLen > 0)) {
		return splitRow(row, []byte(r.FieldDelimiter), r.TrimFunc), readErr
	}
	return nil, readErr
}

func splitRow(row, delimiter []byte, trimFunc func(rune) bool) []string {
	var records []string
	bRecords := bytes.Split(row, delimiter)
	for _, record := range bRecords {
		if trimFunc != nil {
			record = bytes.TrimFunc(record, trimFunc)
		}
		records = append(records, string(record))
	}
	return records
}

func readToDelimiter(src, buffer, delimiter []byte) ([]byte, []byte, int, bool) {
	var dst []byte
	for i, b := range src {
		if b == delimiter[len(buffer)] {
			buffer = append(buffer, b)
		} else {
			if len(buffer) > 0 {
				dst = append(dst, buffer[0])
				buffer = buffer[1:]
				for len(buffer) > 0 && !bytes.HasPrefix(delimiter, buffer) {
					dst = append(dst, buffer[0])
					buffer = buffer[1:]
				}
				if b == delimiter[len(buffer)] {
					buffer = append(buffer, b)
				} else {
					dst = append(dst, b)
				}
			} else {
				dst = append(dst, b)
			}
		}
		if bytes.Equal(delimiter, buffer) {
			return dst, nil, i + 1, true
		}
	}
	return dst, buffer, len(src), false
}
